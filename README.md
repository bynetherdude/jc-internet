# JC-INTERNET
This repository contains the personal home-/resume page of Janis Christen.

## Project Information
### Tooling
- ReactJS
- TypeScript
- SCSS
- Yarn package manager
- Vite build tool

### TODO List
- Finish home page
- Add auto deployment