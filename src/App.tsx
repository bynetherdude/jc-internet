import './App.scss';
import Hero from "./components/module/hero/Hero";
function App() {
  return (
    <div className="App">
        <div>Nav</div>
        <Hero />
        {/*Links "About" Sektion -> Kurze Beschreibung über mich*/}
        {/*Rechts "Tools" Sektion -> Klassische Icon-Darstellung meiner Technologien*/}
        <p>About</p>
        <p>Tools</p>
        {/*Unten rechts noch Gitlab und GitHub verlinken mit Icons*/}
    </div>
  )
}

export default App
