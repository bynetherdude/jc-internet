import './Hero.scss';
import React from "react";

const Hero = () => {
    const baseClassName = 'm-hero';

    return (
        <div className={`${baseClassName}-wrapper`}>
            <div className='inner-wrapper'>
                <div className='title'>
                    <div className='line' />
                    <h1>Janis Christen</h1>
                    <div className='line' />
                </div>
                <h2 className='subtitle'>Web-Entwickler aus Bern</h2>
                <img src='/portrait.png' className='image' alt='Portrait Janis Christen' />
            </div>
        </div>
    );
}

export default Hero;